<?php

namespace Enum;

use MyCLabs\Enum\Enum;

/*
*   Author: Juan Marcos Vallejo
*   Date: 11/29/2020
*   Contact: jamava.1994@gmail.com
*/

class UserStatus extends Enum
{
    const UNLOCKED = 0;
    const LOCKED = 1;
}
